BEGIN {
	a[1] = ""
	gap = ""
	sep = "  "
	indentation_level = 0
	skip_main = 0
	i = 0
}

/{[ \t]*$/ {
	skip_main = 1
	if(indentation_level > 0) {
		gsub(/^[ \t][ \t]*/, "")
		for(i = 0; i < indentation_level; i++) {
			$0 = sep $0
		}
	}
	indentation_level++
	gsub(/{[ \t]*$/, "")
}

/^[ \t]*}[ \t]*$/ {
	indentation_level--
	if(indentation_level < 0) {
		indentation_level = 0
	}
	gsub(/^[ \t]*}[ \t]*$/, "")
}

{
	if(skip_main) {
		skip_main = 0
	}
	else {
		if(indentation_level > 0) {
			gsub(/^[ \t][ \t]*/, "")
			for(i = 0; i < indentation_level; i++) {
				$0 = sep $0
			}
		}
	}
	print
}
