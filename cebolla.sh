set -x
find . -type f | grep '\.cepa$' | {
	while read f ; do
		AWKPATH="$AWKPATH:$PATH" awk -f cebolla.awk <$(realpath $f) >$(realpath $f | sed 's/.cepa$/.nim/')
	done
}
