all: install

install:
	cp cebolla.sh cebolla
	chmod +x cebolla
	cp cebolla cebolla.awk $(HOME)/bin

.PHONY: all install
